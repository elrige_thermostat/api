package com.elrige.thermostat.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.elrige.thermostat.services.sensor.Sensor;

import lombok.Getter;

@Entity
@Table(name = "measurement")
public class Measurement implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Getter
	@Column(nullable = false, unique = true)
	private String uuid;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Sensor sensor;

	@Getter
	@Column(nullable = false)
	private float temperature;

	@Getter
	@Column(nullable = false)
	private float humidity;

	protected Measurement() {
		// no-args constructor required by JPA spec
		// this one is protected since it shouldn't be used directly
	}

	public Measurement(Sensor sensor, float temperature, float humidity) {
		this.uuid = UUID.randomUUID().toString();
		this.sensor = sensor;
		this.temperature = temperature;
		this.humidity = humidity;
	}

}
