package com.elrige.thermostat.entities;

import lombok.Getter;

@Getter
public class PayloadField {

	private boolean state;

	public PayloadField(boolean state) {
		this.state = state;
	}
}
