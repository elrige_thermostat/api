package com.elrige.thermostat.entities;

import com.elrige.thermostat.services.sensor.Sensor;

public enum ThermostatMode {

	PRESENCE {
		public int requiredTemp(Sensor sensor) {
			return sensor.getTempPresence();
		}
	},
	ABSENCE {
		public int requiredTemp(Sensor sensor) {
			return sensor.getTempAbsence();
		}
	},
	UNFROST {
		public int requiredTemp(Sensor sensor) {
			return sensor.getTempUnfrost();
		}
	},
	;

	public abstract int requiredTemp(Sensor sensor);
}
