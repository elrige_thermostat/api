package com.elrige.thermostat.controllers;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.stereotype.Component;

import com.elrige.thermostat.entities.Measurement;
import com.elrige.thermostat.services.MeasurementService;
import com.elrige.thermostat.services.sensor.Sensor;
import com.elrige.thermostat.services.sensor.SensorService;

@Component
public class MeasurementController extends AbstractController {

	private MeasurementService service;
	private SensorService sensorService;

	MeasurementController(MeasurementService service, SensorService sensorService) {
		this.service = service;
		this.sensorService = sensorService;
	}

	@GET
	@Path(Route.MEASUREMENTS)
	public List<Measurement> getMeasurements(@PathParam("uuid") String sensorUuid) {

		Sensor sensor = sensorService.getSensor(sensorUuid);
		return service.getMeasurements(sensor.getId());
	}

}
