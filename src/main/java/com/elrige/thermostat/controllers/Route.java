package com.elrige.thermostat.controllers;

public class Route {

	private Route() {
		throw new IllegalStateException("Utility class");
	}

	public static final String SENSORS = "/sensors";
	public static final String SENSOR = SENSORS + "/{uuid}";

	public static final String MEASUREMENTS = SENSOR + "/measurements";

	public static final String TTNS = "/ttns";
}
