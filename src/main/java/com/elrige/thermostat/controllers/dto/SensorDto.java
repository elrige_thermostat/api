package com.elrige.thermostat.controllers.dto;

import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.elrige.thermostat.entities.ThermostatMode;
import com.elrige.thermostat.services.sensor.Sensor;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

import lombok.Getter;

public class SensorDto extends GenericDto {

	@Getter
	private String name;
	@Getter
	private ThermostatMode mode;
	@Getter
	private int tempPresence;
	@Getter
	private int tempAbsence;
	@Getter
	private int tempUnfrost;

	public SensorDto(Sensor sensor) {
		super(sensor.getUuid());
		this.name = sensor.getName();
		this.mode = sensor.getMode();
		this.tempPresence = sensor.getTempPresence();
		this.tempAbsence = sensor.getTempAbsence();
		this.tempUnfrost = sensor.getTempUnfrost();
	}

	public static List<SensorDto> map(List<Sensor> sensors) {

		return Lists.transform(sensors, new Function<Sensor, SensorDto>() {

			@Override
			public @Nullable SensorDto apply(@Nullable Sensor sensor) {
				return new SensorDto(sensor);
			}
		});
	}
}
