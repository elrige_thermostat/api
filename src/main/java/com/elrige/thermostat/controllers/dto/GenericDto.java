package com.elrige.thermostat.controllers.dto;

import lombok.Getter;

public class GenericDto {

	@Getter
	private String uuid;

	public GenericDto(String uuid) {
		this.uuid = uuid;
	}

}
