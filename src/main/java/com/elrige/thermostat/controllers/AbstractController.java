package com.elrige.thermostat.controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
abstract class AbstractController {

	public <T> T patch(T object, Map<String, Object> updates) throws PatchException {

		for (Entry<String, Object> entry : updates.entrySet()) {

			try {

				Field field = object.getClass().getDeclaredField(entry.getKey());
				field.setAccessible(true);

				Object value = entry.getValue();
				if (field.getType().isEnum()) {
					Method valueOf = field.getType().getMethod("valueOf", String.class);
					value = valueOf.invoke(null, entry.getValue());
				}

				field.set(object, value);
				field.setAccessible(false);

			} catch (NoSuchFieldException | NoSuchMethodException | SecurityException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				throw new PatchException(e);
			}
		}
		return object;
	}

}
