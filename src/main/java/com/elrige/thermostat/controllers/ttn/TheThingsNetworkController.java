package com.elrige.thermostat.controllers.ttn;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import com.elrige.thermostat.controllers.Route;
import com.elrige.thermostat.entities.Measurement;
import com.elrige.thermostat.entities.PayloadField;
import com.elrige.thermostat.services.MeasurementService;
import com.elrige.thermostat.services.sensor.Sensor;
import com.elrige.thermostat.services.sensor.SensorService;
import com.elrige.thermostat.services.ttn.TheThingsNetworkService;

@Component
@Path(Route.TTNS)
public class TheThingsNetworkController {

	private TheThingsNetworkService service;
	private MeasurementService measurementService;
	private SensorService sensorService;

	TheThingsNetworkController(TheThingsNetworkService service, MeasurementService measurementService,
			SensorService sensorService) {
		this.service = service;
		this.measurementService = measurementService;
		this.sensorService = sensorService;
	}

	@GET
	public String test() {

		service.sendCommand(new Sensor("thermo", "test"), new PayloadField(true));
		return "OK";
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void addMeasurement(TtnUplink ttnUplink) {

		Sensor sensor = sensorService.getSensorByDevId(ttnUplink.getDevId());
		if (sensor == null) {
			throw new ResourceAccessException("Sensor is null");
		}
		
		measurementService.createMeasurement(new Measurement(
				sensor,
				ttnUplink.getPayloadFields().getTemperature(),
				ttnUplink.getPayloadFields().getHumidity()));
	}

}
