package com.elrige.thermostat.controllers.ttn;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
class TtnUplink {

	@JsonProperty("app_id")
	public String appId;
	@JsonProperty("dev_id")
	public String devId;
	@JsonProperty("hardware_serial")
	public String hardwareSerial;
	public Integer port;
	public Integer counter;
	@JsonProperty("is_retry")
	public Boolean isRetry;
	public Boolean confirmed;
	@JsonProperty("payload_fields")
	public PayloadFields payloadFields;
	public Metadata metadata;
	@JsonProperty("downlink_url")
	public String downlinkUrl;

	@Getter
	private static class Gateway {

		@JsonProperty("gtw_id")
		public String gtwId;
		public Integer timestamp;
		public String time;
		public Integer channel;
		public Integer rssi;
		public Integer snr;
		@JsonProperty("rf_chain")
		public Integer rfChain;
		public Float latitude;
		public Float longitude;
		public Integer altitude;

	}

	@Getter
	private static class Metadata {

		public String time;
		public Float frequency;
		public String modulation;
		@JsonProperty("data_rate")
		public String dataRate;
		@JsonProperty("bit_rate")
		public Integer bitRate;
		@JsonProperty("coding_rate")
		public String codingRate;
		public List<Gateway> gateways = null;
		public Float latitude;
		public Float longitude;
		public Integer altitude;

	}

	@Getter
	public static class PayloadFields {
		private Float temperature;
		private Float humidity;
	}

}
