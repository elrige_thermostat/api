package com.elrige.thermostat.controllers;

public class PatchException extends Exception {

	public PatchException(Throwable throwable) {
		super(throwable);
	}

}
