package com.elrige.thermostat.controllers;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.stereotype.Component;

import com.elrige.thermostat.controllers.dto.SensorDto;
import com.elrige.thermostat.services.sensor.Sensor;
import com.elrige.thermostat.services.sensor.SensorService;

@Component
public class SensorController extends AbstractController {

	private SensorService service;

	SensorController(SensorService service) {
		this.service = service;
	}

	@POST
	@Path(Route.SENSORS)
	public SensorDto createSensor() {

		Sensor sensor = new Sensor("Appart", "Confort");
		sensor = service.createSensor(sensor);
		return new SensorDto(sensor);
	}

	@GET
	@Path(Route.SENSORS)
	public List<SensorDto> getSensor() {

		return SensorDto.map(service.getSensors());
	}

	@GET
	@Path(Route.SENSOR)
	public SensorDto getSensor(@PathParam("uuid") String sensorUuid) {

		return new SensorDto(service.getSensor(sensorUuid));
	}

	@PATCH
	@Path(Route.SENSOR)
	public SensorDto partialUpdate(@PathParam("uuid") String sensorUuid, Map<String, Object> updates)
			throws PatchException {

		SensorDto sensorDto = patch(new SensorDto(service.getSensor(sensorUuid)), updates);
		Sensor sensor = service.updateSensor(sensorUuid, sensorDto);

		return new SensorDto(sensor);
	}

}
