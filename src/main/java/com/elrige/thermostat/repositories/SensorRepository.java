package com.elrige.thermostat.repositories;

import org.springframework.data.repository.CrudRepository;

import com.elrige.thermostat.services.sensor.Sensor;

public interface SensorRepository extends CrudRepository<Sensor, Long> {

	Sensor findByUuid(String uuid);

	Sensor findByDevId(String devId);
}
