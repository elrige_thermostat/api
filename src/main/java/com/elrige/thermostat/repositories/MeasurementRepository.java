package com.elrige.thermostat.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.elrige.thermostat.entities.Measurement;

public interface MeasurementRepository extends CrudRepository<Measurement, Long> {

	List<Measurement> findBySensorId(Long sensorId);

	Measurement findTopBySensorIdOrderByIdDesc(Long sensorId);

}
