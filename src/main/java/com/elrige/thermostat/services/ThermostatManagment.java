package com.elrige.thermostat.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elrige.thermostat.entities.Measurement;
import com.elrige.thermostat.services.sensor.Sensor;
import com.elrige.thermostat.services.sensor.SensorService;

@Component
public class ThermostatManagment {

	@Autowired
	private SensorService sensorService;
	@Autowired
	private MeasurementService measurementService;

	public void checkTemperature() {

		Iterable<Sensor> sensors = sensorService.getSensors();
		for (Sensor sensor : sensors) {

			Measurement measurement = measurementService.getLastestMeasurements(sensor.getId());
			if (measurement == null) {
				System.out.println("No measurement yet");
				continue;
			}

			int requiredTemp = sensor.getMode().requiredTemp(sensor);

			System.out.println("requiredTemp : " + requiredTemp);
			System.out.println("measurementTemp : " + measurement.getTemperature());
		}

	}

}
