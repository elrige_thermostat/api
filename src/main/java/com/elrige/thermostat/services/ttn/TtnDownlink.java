package com.elrige.thermostat.services.ttn;

import com.elrige.thermostat.entities.PayloadField;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
class TtnDownlink {

	@JsonProperty(value = "dev_id")
	private String devId; // The device ID
	private int port; // LoRaWAN FPort - SET TO 1
	private boolean confirmed; // Whether the downlink should be confirmed by the device - SET TO FALSE
	@JsonProperty(value = "payload_fields")
	private PayloadField payloadField; // The JSON object to be encoded by your encoder payload function

	TtnDownlink(String devId, PayloadField payloadField) {
		this.devId = devId;
		this.port = 1;
		this.confirmed = false;
		this.payloadField = payloadField;
	}

}
