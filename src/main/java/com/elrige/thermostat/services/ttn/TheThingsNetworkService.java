package com.elrige.thermostat.services.ttn;

import javax.ws.rs.BadRequestException;

import org.springframework.stereotype.Component;

import com.elrige.thermostat.entities.PayloadField;
import com.elrige.thermostat.services.sensor.Sensor;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Component
public class TheThingsNetworkService {

	private static final String TTN_APP_ID = "thermostat";
	private static final String TTN_PROCESS_ID = "messaging";
	private static final String TTN_DEVICE_SECRET = "lyCGzXgmkMwIXwwyM3Z9W3E245H-UE0oEKblqwh4O58";
	private static final String TTN_URL = "https://integrations.thethingsnetwork.org/ttn-eu/api/v2/down/" + TTN_APP_ID
			+ "/" + TTN_PROCESS_ID + "?key=ttn-account-v2." + TTN_DEVICE_SECRET;

	public void saveData() {

	}

	public void sendCommand(Sensor sensor, PayloadField payloadField) {

		TtnDownlink ttnDownlink = new TtnDownlink(sensor.getDevId(), payloadField);

		try {

			HttpResponse<JsonNode> jsonResponse = Unirest.post(TTN_URL)
					.header("accept", "application/json")
					.body(ttnDownlink)
					.asJson();

			if (jsonResponse.getStatus() >= 300) {
				throw new BadRequestException(jsonResponse.getStatusText());
			}

			System.out.println(jsonResponse);

		} catch (UnirestException e) {
			throw new BadRequestException(e);
		}

	}

}
