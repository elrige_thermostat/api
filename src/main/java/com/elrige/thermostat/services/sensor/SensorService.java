package com.elrige.thermostat.services.sensor;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import com.elrige.thermostat.controllers.dto.SensorDto;
import com.elrige.thermostat.repositories.SensorRepository;
import com.google.common.collect.Lists;

@Component
public class SensorService {

	private SensorRepository repository;

	public SensorService(EntityManagerFactory factory, SensorRepository repository) {
		this.repository = repository;
	}

	public Sensor createSensor(Sensor sensor) {

		return repository.save(sensor);
	}

	public List<Sensor> getSensors() {

		return Lists.newArrayList(repository.findAll());
	}

	public Sensor getSensor(String uuid) {

		Sensor sensor = repository.findByUuid(uuid);
		if (sensor == null) {
			throw new ResourceAccessException("Sensor is null");
		}
		return sensor;
	}

	public Sensor getSensorByDevId(String devId) {
		return repository.findByDevId(devId);
	}

	public Sensor updateSensor(String uuid, SensorDto sensorDto) {

		Sensor sensor = getSensor(uuid);
		sensor.setName(sensorDto.getName());
		sensor.setMode(sensorDto.getMode());
		sensor.setTempPresence(sensorDto.getTempPresence());
		sensor.setTempAbsence(sensorDto.getTempAbsence());
		sensor.setTempUnfrost(sensorDto.getTempUnfrost());

		return repository.save(sensor);
	}

}
