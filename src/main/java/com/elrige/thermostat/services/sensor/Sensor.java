package com.elrige.thermostat.services.sensor;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.elrige.thermostat.entities.Measurement;
import com.elrige.thermostat.entities.ThermostatMode;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sensor")
public class Sensor implements Serializable {

	@Getter
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Getter
	@Column(nullable = false, unique = true)
	private String uuid;

	@Getter
	@Column(nullable = false, unique = true)
	private String devId;

	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	@Column(nullable = false)
	private String name;

	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ThermostatMode mode;

	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	@Column(nullable = false)
	private int tempPresence;

	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	@Column(nullable = false)
	private int tempAbsence;

	@Getter
	@Setter(value = AccessLevel.PROTECTED)
	@Column(nullable = false)
	private int tempUnfrost;

	@OneToMany(mappedBy = "sensor")
	private List<Measurement> measurements;

	protected Sensor() {
		// no-args constructor required by JPA spec
		// this one is protected since it shouldn't be used directly
	}

	public Sensor(String name, String devId) {
		this.uuid = UUID.randomUUID().toString();
		this.name = name;
		this.devId = devId;
		this.mode = ThermostatMode.PRESENCE;
		this.tempPresence = 19;
		this.tempAbsence = 15;
		this.tempUnfrost = 10;
	}

	public Sensor(Long id, String uuid, String name, String devId, ThermostatMode mode, int tempPresence,
			int tempAbsence, int tempUnfrost) {

		this.id = id;
		this.uuid = uuid;
		this.name = name;
		this.devId = devId;
		this.mode = mode;
		this.tempPresence = tempPresence;
		this.tempAbsence = tempAbsence;
		this.tempUnfrost = tempUnfrost;
	}

}
