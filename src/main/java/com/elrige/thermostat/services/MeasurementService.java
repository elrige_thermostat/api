package com.elrige.thermostat.services;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.springframework.stereotype.Component;

import com.elrige.thermostat.entities.Measurement;
import com.elrige.thermostat.repositories.MeasurementRepository;

@Component
public class MeasurementService {

	private MeasurementRepository repository;

	public MeasurementService(MeasurementRepository repository, EntityManagerFactory factory) {
		this.repository = repository;
	}

	public Measurement createMeasurement(Measurement measurement) {

		return repository.save(measurement);
	}

	public List<Measurement> getMeasurements(Long sensorId) {

		return repository.findBySensorId(sensorId);
	}

	public Measurement getLastestMeasurements(Long sensorId) {

		return repository.findTopBySensorIdOrderByIdDesc(sensorId);
	}
}
