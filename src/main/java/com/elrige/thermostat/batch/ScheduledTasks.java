package com.elrige.thermostat.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.elrige.thermostat.services.ThermostatManagment;

@Component
public class ScheduledTasks {

	@Autowired
	private ThermostatManagment thermostatManagment;

	@Scheduled(fixedRate = 50000)
	public void checkTemperature() {
		thermostatManagment.checkTemperature();
	}

}
