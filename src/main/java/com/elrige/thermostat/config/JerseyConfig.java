package com.elrige.thermostat.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.elrige.thermostat.controllers.MeasurementController;
import com.elrige.thermostat.controllers.SensorController;
import com.elrige.thermostat.controllers.ttn.TheThingsNetworkController;

@Configuration
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {

		register(SensorController.class);
		register(TheThingsNetworkController.class);
		register(MeasurementController.class);

	}
}
